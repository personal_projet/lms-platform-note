import {prisma} from "@/lib/prisma";

interface GetCourseQueryParams {
  courseId: string;
  userId: string;
  userPage: number;
}

export const getAdminCourse = async ({courseId, userId, userPage}: GetCourseQueryParams) => {
  const course = await prisma.course.findUnique({
    where: {
      ownerId: userId,
      id: courseId,
    },
    select: {
      id: true,
      state: true,
      image: true,
      name: true,
      description: true,
      users: {
        take: 5,
        skip: Math.max(0, userPage  * 5),
        select: {
          canceledAt: true,
          id: true,
          user: {
            select: {
              id: true,
              image: true,
              email: true
            },
          },
        },
      },
      _count: {
        select: {
          lessons: true,
          users: true
        },
      },
    },
  });

  const users = course?.users.map((user) => {
    return {
      canceled: !!user.canceledAt,
      ...user.user,
    };
  });

  return {
    ...course,
    users,
  }
}
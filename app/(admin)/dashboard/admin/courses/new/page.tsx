import {getRequiredAuthSession} from "@/lib/auth";
import {Layout, LayoutContent, LayoutHeader, LayoutTitle} from "@/components/layout/layout";
import {Card, CardContent} from "@/components/ui/card";
import {CourseForm} from "../[courseId]/edit/course-form";

export default async function CoursePage() {
  await getRequiredAuthSession();

  return(
    <Layout>
      <LayoutHeader>
        <LayoutTitle>Create courses</LayoutTitle>
      </LayoutHeader>
      <LayoutContent>
        <Card className="flex-[2]">
          <CardContent className="mt-6">
            <CourseForm/>
          </CardContent>
        </Card>
      </LayoutContent>
    </Layout>
  )
}
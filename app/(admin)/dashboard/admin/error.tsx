"use client";

import {useEffect} from "react";
import {Card, CardDescription, CardFooter, CardHeader, CardTitle} from "@/components/ui/card";
import {LoginButton} from "@/components/features/auth/login-button";

interface ErrorProps {
  error: Error & { digest?: string };
  reset: () => void;
}

export default function Error({error, reset}: ErrorProps) {
  useEffect(() => {
    console.error(error)
  }, [error])

  return (
    <Card>
      <CardHeader>
        <CardTitle>
          Sorry, an error occurred while processing your request.
        </CardTitle>
        <CardDescription>
          Try to login again or contact support.
        </CardDescription>
        <CardFooter>
          <LoginButton/>
        </CardFooter>
      </CardHeader>
    </Card>
  );
}
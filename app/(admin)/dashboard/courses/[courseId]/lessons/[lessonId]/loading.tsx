import {LessonSkeleton} from "./lesson-skeleton";

export default function loading() {
  return <LessonSkeleton />;
}
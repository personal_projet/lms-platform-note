import {getAuthSession} from "@/lib/auth";
import {getCourse} from "../course.query";
import {notFound} from "next/navigation";
import {LessonNavigationCard} from "./lesson-navigation-card";

interface LessonsNavigationProps {
  courseId: string;
}

export const LessonsNavigation = async ({courseId}: LessonsNavigationProps) => {
  const session = await getAuthSession();
  const course = await getCourse({
    courseId: courseId,
    userId: session?.user.id,
  });

  if (!course) {
    return notFound();
  }

  return <LessonNavigationCard course={course}/>;
}
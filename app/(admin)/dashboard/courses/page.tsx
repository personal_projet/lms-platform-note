import {getAuthSession} from "@/lib/auth";
import {Layout, LayoutContent, LayoutHeader, LayoutTitle} from "@/components/layout/layout";
import {NotAuthenticatedCard} from "@/components/features/errors/not-authentificated-card";
import {getCourses} from "./course.query";
import {Alert, AlertDescription, AlertTitle} from "@/components/ui/alert";
import {AlertTriangle} from "lucide-react";
import {PaginationButton} from "@/components/features/pagination/pagination-button";
import {CourseCard} from "./course-card";

interface CoursePageProps {
  searchParams: { [key: string]: string | string[] | undefined }
}
export default async function CoursesPage({searchParams}: CoursePageProps) {

  const session = await getAuthSession();
  if (!session) {
    return <NotAuthenticatedCard/>
  }

  const page = Number(searchParams.currentPage ?? 0) ?? 0;
  const { courses, totalCourses} = await getCourses({
    userId: "clowrt4le000cyzdv3ivnyeip",
    page
  });

  console.log(courses)


  return (
    <Layout>
      <LayoutHeader>
        <LayoutTitle>Your courses</LayoutTitle>
      </LayoutHeader>
      <LayoutContent>
        <div className="grid grid-cols-1 gap-4 md:grid-cols-2 2xl:grid-cols-3">
          {courses.map((course) => (
            <CourseCard course={course} key={course.id} />
          ))}
        </div>
        {courses.length === 0 ? (
          <Alert>
            <AlertTriangle />
            <AlertTitle>You are not enrolled in any courses yet.</AlertTitle>
            <AlertDescription>
              Please go to the explorer page to find a course.
            </AlertDescription>
          </Alert>
        ) : (
          <PaginationButton
            baseUrl={`/courses`}
            currentPage={page}
            totalPage={totalCourses}
          />
        )}
      </LayoutContent>
    </Layout>
  )
}
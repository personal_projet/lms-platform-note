import {prisma} from '@/lib/prisma';
import {Prisma} from '@prisma/client';

const COUNT_BY_PAGE = 10;

interface GetCourseArgs {
  userId?: string | undefined;
  page?: number;
}

export const getCourses = async ({userId, page = 0}: GetCourseArgs) => {
  const whereQuery = (
    userId
      ? {
        users: {
          some: {
            userId,
            canceledAt: null,
          },
        },
      }
      : {
        state: 'PUBLISHED',
      }
  ) satisfies Prisma.CourseWhereInput;

  const totalCourses = await prisma.course.count({
    where: whereQuery,
  });

  console.log('totalCourses', totalCourses)

  const courses = await prisma.course.findMany({
    where: whereQuery,
    select: {
      name: true,
      image: true,
      description: true,
      id: true,
      owner: {
        select: {
          image: true,
          name: true,
        },
      },
    },
    take: COUNT_BY_PAGE,
    skip: Math.max(0, page * COUNT_BY_PAGE),
  });

  return {
    courses,
    totalCourses: Math.floor(totalCourses / COUNT_BY_PAGE),
  };
};

export type CoursesCard = Prisma.PromiseReturnType<
  typeof getCourses
>['courses'][number];
import {CoursesCard} from "./course.query";
import {Typography} from "@/components/ui/typography";
import {Avatar, AvatarFallback, AvatarImage} from "@/components/ui/avatar";
import {Card, CardHeader, CardTitle} from "@/components/ui/card";
import Link from "next/link";

interface CourseCardProps {
  course: CoursesCard
}
export const CourseCard = ({course} : CourseCardProps) => {
  return(
    <Link href={`/dashboard/courses/${course.id}`}>
      <Card className="hover:bg-accent">
        <CardHeader className="flex flex-row gap-3 space-y-0">
          <Avatar className="h-14 w-14 rounded">
            <AvatarFallback>{course.name[0]}</AvatarFallback>
            {course.image ? <AvatarImage src={course.image} /> : null}
          </Avatar>
          <div className="flex flex-col gap-3">
            <CardTitle>{course.name}</CardTitle>
            <div className="flex flex-row gap-2">
              <Avatar className="h-8 w-8">
                <AvatarFallback>{course.name[0]}</AvatarFallback>
                {course.owner.image ? (
                  <AvatarImage src={course.owner.image} />
                ) : null}
              </Avatar>
              <Typography variant="large" className=" text-muted-foreground">
                {course.owner.name}
              </Typography>
            </div>
          </div>
        </CardHeader>
      </Card>
    </Link>  )
}
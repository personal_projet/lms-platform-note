"use client";



import {PropsWithChildren} from "react";
import {usePathname, useRouter} from "next/navigation";
import {Dialog, DialogContent} from "@/components/ui/dialog";

export type CourseDialogProps = PropsWithChildren;

export const CourseDialog = (props : CourseDialogProps) => {
  const router = useRouter();
  const pathname = usePathname();

  const isCoursePage = pathname?.split('/').filter(Boolean).length === 2;

  return(
    <Dialog
      open={isCoursePage}
      onOpenChange={() => {router.back()}}
    >
      <DialogContent
        className="max-h-screen max-w-3xl overflow-auto"
      >
        {props.children}
      </DialogContent>
    </Dialog>
  )
}
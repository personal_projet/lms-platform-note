import { DialogHeader, DialogTitle } from '@/components/ui/dialog';
import { getAuthSession } from '@/lib/auth';
import { notFound } from 'next/navigation';
import { Suspense } from 'react';
import {CourseDialog} from "./course-dialog";
import {CoursePlaceholder} from "../../../(admin)/dashboard/courses/[courseId]/course-placeholder";
import {getCourse} from "../../../(admin)/dashboard/courses/[courseId]/course.query";
import {Course} from "../../../(admin)/dashboard/courses/[courseId]/course";

export default async function CoursePage({
                                           params,
                                         }: {
  params: {
    courseId: string;
  };
}) {
  if (!params.courseId) {
    notFound();
  }

  return (
    <CourseDialog>
      <Suspense
        fallback={
          <>
            <DialogHeader>
              <DialogTitle>Loading...</DialogTitle>
            </DialogHeader>
            <CoursePlaceholder />
          </>
        }
      >
        <CourseWithData courseId={params.courseId} />
      </Suspense>
    </CourseDialog>
  );
}

const CourseWithData = async ({ courseId }: { courseId: string }) => {
  const session = await getAuthSession();
  const course = await getCourse({
    courseId: courseId,
    userId: session?.user.id,
  });

  await new Promise((resolve) => setTimeout(resolve, 5000));

  if (!course) {
    notFound();
  }

  return (
    <>
      <DialogHeader>
        <DialogTitle>{course.name}</DialogTitle>
      </DialogHeader>
      <Course course={course} userId={session?.user.id} />
    </>
  );
};
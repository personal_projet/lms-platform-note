import Markdown from 'react-markdown';

interface MarkdownProsedProps {
  markdown: string;
}

export const MarkdownProse = ({ markdown }: MarkdownProsedProps) => {
  return (
    <Markdown className="prose dark:prose-invert lg:prose-lg">
      {markdown}
    </Markdown>
  );
};
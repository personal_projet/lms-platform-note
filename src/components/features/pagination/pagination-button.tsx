"use client";

import {useRouter} from "next/navigation";
import {Button} from "@/components/ui/button";

interface PaginationButtonProps {
  totalPage: number;
  currentPage: number;
  baseUrl: string;
}

export const PaginationButton = ({totalPage, currentPage, baseUrl}: PaginationButtonProps) => {
  const router = useRouter();

  const handleClick = (num: number) => {
    const searchParams = new URLSearchParams({currentPage: String(currentPage + num)});
    const url = `${baseUrl}?${searchParams.toString()}`;
    router.push(url);
  }

  return (
    <div className="flex gap-2">
      <Button
        variant="outline"
        size="sm"
        onClick={() => handleClick(-1)}
      >
        Previous
      </Button>
      <Button
        variant="outline"
        size="sm"
        onClick={() => handleClick(1)}
        >
        Next
      </Button>
    </div>
  )
}
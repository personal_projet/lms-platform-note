import { getAuthSession } from '@/lib/auth';
import {LoginButton} from "@/components/features/auth/login-button";
import {LoggedInButton} from "@/components/features/auth/logged-in-button";

export type AuthButtonProps = {};

export const ButtonAuth = async (props: AuthButtonProps) => {
  const session = await getAuthSession();

  const user = session?.user;

  if (!user) {
    return <LoginButton />;
  }

  return <LoggedInButton user={user} />;
};
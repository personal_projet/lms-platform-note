"use client";

import React from 'react';
import {useMutation} from "@tanstack/react-query";
import {signIn} from "next-auth/react";
import {Button} from "@/components/ui/button";
import {Loader, LogIn} from "lucide-react";

export const LoginButton = () => {
  const mutation = useMutation({
    mutationFn: async () => signIn(),
  });
  return(
    <Button
      variant="outline"
      size="sm"
      onClick={() => {
        mutation.mutate();
      }}
      disabled={mutation.isPending}
    >
      {mutation.isPending ? (
        <Loader className="mr-2" size={12} />
      ) : (
        <LogIn className="mr-2" size={12} />
      )}
      Login
    </Button>
  )
}
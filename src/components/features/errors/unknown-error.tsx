import {LoginButton} from "@/components/features/auth/login-button";
import {Card, CardFooter, CardHeader, CardTitle} from "@/components/ui/card";

export const UnknownError = () => {
  return(
    <Card className="m-auto mt-4 max-w-lg">
      <CardHeader>
        <CardTitle>You need to be logged in to view this page</CardTitle>
      </CardHeader>
      <CardFooter>
        <LoginButton />
      </CardFooter>
    </Card>
  )
}
// src/components/layout/header.tsx
import Link from 'next/link';
import { Typography } from '../ui/typography';
import {SiteConfig} from "@/lib/site-config";
import {ThemeToggle} from "@/components/theme/ThemeToggle";
import Image from "next/image";
import {ButtonAuth} from "@/components/features/auth/button-auth";

export function Header() {
  return (
    <header className="bg-background sticky top-0 z-40 w-full border-b">
      <div className="container flex h-16 items-center space-x-4 sm:justify-between sm:space-x-0">
        <div className="flex gap-2 items-center">
          <Image src="/images/logo.png" width={50} height={34} alt="App Logo"/>
          <Typography variant="h3" as={Link} href="/">
            {SiteConfig.title}
          </Typography>
        </div>

        <div className="flex flex-1 items-center justify-end space-x-4">
          <nav className="flex items-center space-x-1">
            <ThemeToggle />
            <ButtonAuth/>
          </nav>
        </div>
      </div>
    </header>
  );
}

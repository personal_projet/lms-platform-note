import type { ComponentPropsWithoutRef } from 'react';
import {cn} from "@/lib/utils";
import {Typography} from "@/components/ui/typography";

interface LayoutProps extends ComponentPropsWithoutRef<'div'> {}
interface LayoutTitleProps extends ComponentPropsWithoutRef<'h1'> {}
interface LayoutDescriptionProps extends ComponentPropsWithoutRef<'p'> {}

export const Layout = (props: LayoutProps) => {
  return(
    <div
      className={cn(
        "max-w-3xl flex-wrap w-full flex gap-4 m-auto px-4 mt-4",
        props.className
      )}
      {...props}
    >
    </div>
  );
};

export const LayoutHeader = (props: LayoutProps) => {
  return (
    <div
      {...props}
      className={cn(
        'flex items-start gap-1 flex-col w-full md:flex-1 min-w-[200px]',
        props.className
      )}
    />
  );
};

export const LayoutTitle = (props: LayoutTitleProps) => {
  return <Typography {...props} variant="h2" className={cn(props.className)}/>;
};

export const LayoutDescription = (props: LayoutDescriptionProps) => {
  return <Typography {...props} className={cn(props.className)}/>;
};

export const LayoutActions = (props: LayoutProps) => {
  return <div {...props} className={cn(props.className, "flex items-center")}/>;
};

export const LayoutContent = (props: LayoutProps) => {
  return <div {...props} className={cn("w-full", props.className)}/>;
};